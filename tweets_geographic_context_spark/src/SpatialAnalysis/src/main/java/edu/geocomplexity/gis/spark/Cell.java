package edu.geocomplexity.gis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cell implements Serializable{
	
	public long x;
	public long y;
	public long z;
	
	public Cell() {
	
	}
	
	public Cell(long x, long y, long z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static Cell parse(String str) {
		String[] arr = str.split(",");
		return new Cell(Long.parseLong(arr[0]),Long.parseLong(arr[1]),Long.parseLong(arr[2]));
	}

	@Override
	public String toString() {
		return  x + ","  + y + "," + z;
	}
	
	public List<Cell> getNeighbours(){
		List<Cell> neighbours = new ArrayList<Cell>();
		for(int i=-1;i<=1;i++){
			for(int j=-1;j<=1;j++){
				for(int k=-1;k<=1;k++){
					neighbours.add(new Cell(x+i,y+j,z+k));
				}
			}
		}
		return neighbours;
	}
	
	public List<Cell> getRoundMeighbors(int round){
		List<Cell> neighbours = new ArrayList<Cell>();
		for(int i=-round;i<=round;i++){
			for(int j=-round;j<=round;j++){
				for(int k=-round;k<=round;k++){
					if(i==-round||i==round||j==-round||j==round||k==-round||k==round){
						if(x+i>=0&&y+j>=0&&z+k>=0){
							neighbours.add(new Cell(x+i,y+j,z+k));
						}
					
					}
				}
			}
		}
		return neighbours;
	}
	

	
	public static void main(String[] args) {
		Cell cell = new Cell(1,1,1);
		 List<Cell> cells = cell.getRoundMeighbors(1);
		 for(int i=0;i<cells.size();i++){
			 System.out.println(cells.get(i));
		 }
	}
	
}
