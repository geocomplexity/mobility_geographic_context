package edu.geocomplexity.gis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



import com.esri.core.geometry.Point;

public class TripRecord {
	
	public Date pickUpTime;
	public Date dropoffTime;
	public Point picupLoc;
	public Point dropoffLoc;
	
	public static TripRecord parse(String record) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String[] fields = record.split(",");
	
		try {
			TripRecord trip = new TripRecord();
			trip.pickUpTime = format.parse(fields[1]);
			trip.dropoffTime = format.parse(fields[2]);
			trip.picupLoc = new Point(Double.parseDouble(fields[5]),Double.parseDouble(fields[6]),trip.pickUpTime.getTime());
			trip.dropoffLoc = new Point(Double.parseDouble(fields[9]),Double.parseDouble(fields[10]),trip.dropoffTime.getTime());
			return trip;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}	
	}
	
	
	
	@Override
	public String toString() {
		return "Trip [pickUpTime=" + pickUpTime + ", dropoffTime="
				+ dropoffTime + ", picupLoc=" + picupLoc.getX()+","+picupLoc.getY() + ", dropoffLoc="
				+ dropoffLoc.getX() +","+ dropoffLoc.getY()+"]";
	}


	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		String str = "2,2015-01-01 00:35:13,2015-01-01 00:41:04,N,1,-73.917129516601562,40.764888763427734,-73.927978515625,40.761470794677734,5,1.18,6.5,0.5,0.5,1.4,0,,0.3,9.2,1,1,,";
		System.out.println(TripRecord.parse(str));
		
	
	}
	
	

}
