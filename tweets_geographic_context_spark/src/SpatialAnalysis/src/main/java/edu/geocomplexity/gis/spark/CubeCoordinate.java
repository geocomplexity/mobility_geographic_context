package edu.geocomplexity.gis;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.Point;

public class CubeCoordinate {
	
	public static Point originalPosition = new Point(180, 90);
	public static Date originalTime;
	public static double timeStep;   // time step size is defined in days
	public static double cellSize;   // cell size in degrees
	
	static{
		 originalPosition = new Point(180, 90);
		 timeStep = 30;
		 cellSize =  0.5;
		 try {
			originalTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse("2015-01-01 00:00:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * ���ÿռ�ֱ��ʣ�����Ϊ�ֱ��ʲ㼶
	 */
	public static void SetSpatialResolution(int level)
	{
		cellSize =  2 * 180 / Math.pow(2, level);
	}
	
	/*
	 * ����ʱ��ֱ���,����Ϊ��λ
	 */
	public static void SetTimeResolution(double _timeStep)
	{
		timeStep = _timeStep;
	}
	
	
	public static Cell GetPicUpCell(TripRecord record){
		return GetPointCell(record.picupLoc);
	}
	
	public static Cell GetDropoffCell(TripRecord record){
		return GetPointCell(record.dropoffLoc);
	}
	

	/*
	 * ��ȡp���ڵĵ�Ԫ����
	 */
	public static Cell GetPointCell(Point p,boolean ignoreZ){
		Cell cell = new Cell();
	    cell.x = (long) ((p.getX()+originalPosition.getX())/cellSize);
	    cell.y = (long) ((originalPosition.getY()-p.getY())/cellSize);

		if(ignoreZ){
			cell.z = 0;
		}

		return cell;
	}

	public static Cell GetPointCell(Point p){
		Cell cell = new Cell();
		cell.x = (long) ((p.getX()+originalPosition.getX())/cellSize);
		cell.y = (long) ((originalPosition.getY()-p.getY())/cellSize);

		cell.z = (long) ((p.getZ()-originalTime.getTime())/(1000*60*60*24*CubeCoordinate.timeStep));


		return cell;
	}
	
	/*
	 * ���ݾ��Σ���ȡ�þ��ο�Խ�ĵ�ǰ��������ϵ�еĸ�������
	 */
	public static List<Cell> GetCellRange(Envelope env){
		Cell lowleftCell = GetPointCell(env.getLowerLeft(),true);
		Cell upperRightCell = GetPointCell(env.getUpperRight(),true);
		List<Cell> cellRange = new ArrayList<Cell>();
		for(long i=lowleftCell.x;i<=upperRightCell.x;i++){
			for(long j=upperRightCell.y;j<=lowleftCell.y;j++){
				cellRange.add(new Cell(i,j,0));
			}
		}
		
		return cellRange;
	}
	
	public static Envelope GetEnvelopeByCell(Cell cell){
		double minx = ((double)cell.x)*cellSize-originalPosition.getX();
		double maxx = ((double)cell.x+1)*cellSize-originalPosition.getX();
		double miny = originalPosition.getY()-((double)cell.y+1)*cellSize;
		double maxy = originalPosition.getY()-((double)cell.y)*cellSize;
	
		return new Envelope(minx,miny,maxx,maxy);
		
	}
	
	public static void main(String[] args) throws IOException 
	{
		Envelope env = new Envelope(2.25,2.25,2.35,2.35);
		List<Cell> cells = GetCellRange(env);
		for(int i=0;i<cells.size();i++){
		   Envelope env1 = GetEnvelopeByCell(cells.get(i));
		   System.out.println(env1.getXMin()+" "+env1.getYMin()+" "+env1.getXMax()+" "+env1.getYMax());
		}
	}
	
		
}


