package edu.geocomplexity.gis.spark;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import scala.Tuple2;

import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.Envelope2D;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.QuadTree;
import com.esri.core.geometry.QuadTree.QuadTreeIterator;

import edu.geocomplexity.gis.Cell;
import edu.geocomplexity.gis.CubeCoordinate;

public class cellMapper {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub


        CubeCoordinate.cellSize = 0.05;

        File file = new File("");
        BufferedReader reader = null;
        reader = new BufferedReader(new FileReader(file));
        String tempString = null;

        Map<String,List<Geometry>> geometryMap = new HashMap<String,List<Geometry>>();

        while ((tempString = reader.readLine()) != null) {
            String[] strArr = tempString.split("\t");
            String wkt = strArr[strArr.length-1];
            Geometry geo = GeometryEngine.geometryFromWkt(wkt, 0, Geometry.Type.Point);


            Cell cell = CubeCoordinate.GetPointCell((Point)geo);

            if(!geometryMap.containsKey(cell.toString())){
                geometryMap.put(cell.toString(), new ArrayList<Geometry>());
            }
            geometryMap.get(cell.toString()).add(geo);

        }
        reader.close();


        for(Map.Entry<String,List<Geometry>> entry:geometryMap.entrySet()){
            System.out.println(entry.getKey());

        }
    }
}
